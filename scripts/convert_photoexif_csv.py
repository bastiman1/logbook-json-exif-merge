from datetime import datetime
import math
import json
import csv
import sys
import os
import re
from fractions import Fraction
version = "logbook_json_exif_merge v0.1"

def get_photoexifcsv_filename(filepath):
   for x in os.listdir(filepath):
           if x.endswith("csv"):
               csv_file = x
   return(csv_file)


def load_photoexifcsv(csv_file):
    with open(csv_file,newline='') as file:
        photoexif_csv = csv.DictReader(file)
        shots = list(photoexif_csv)

    return(shots)

def load_file_dict(filepath):
    file_dict ={}
    for x in os.listdir(filepath):
        if x.endswith("tif"):
            shot_number = str(int(list(filter(None,re.findall("\d*",x)))[0]))
            file_dict[shot_number] = x
    return(file_dict)

def fnumber_to_FNumber(fnumber):
    return(fnumber)

def date_to_DateTimeOriginal(date_str):
    exif_date = datetime.strptime(date_str,"%d/%m/%Y %H:%M").strftime("%Y:%m:%d %H:%M:%S")
    return(exif_date)


def exposure_time_to_ExposureTime(time):
    try: 
      float_time = float(sum(Fraction(s) for s in time.replace("s","").split()))
      return(float_time)
    except ValueError:
      return(0)  


def focal_length_to_FocalLength(focal_length):
    return(focal_length)

def focal_length_to_FocalLengthIn35mmFormat(focal_length,film_format):
    if(film_format == "135"):
        return(focal_length)
    elif(film_format == "6x7"):
        return(int(focal_length) * 0.4692)
    else:
        return("null")

def dec_to_dms(dec):

    d = math.floor(dec)
    m = math.floor((dec-d) * 60)
    s = round((dec - d - m/60) * 3600)

    return(str(d) + "deg " + str(m) + "' " + str(s) + '" ')

def lat_dec_to_lat_dms(lat):
    if(lat > 0):
        dms_lat = dec_to_dms(lat) + "N"
    else:
        dms_lat = dec_to_dms(lat) + "S"
    return(dms_lat)

def lon_dec_to_lon_dms(lon):
    if(lon > 0):
        dms_lon = dec_to_dms(lon) + "E"
    else:
        dms_lon = dec_to_dms(lon) + "W"
    return(dms_lon)

def lat_dec_to_latref(lat):
    if(lat > 0):
        gpsref = "North"
    else:
        gpsref = "South"
    return(gpsref)

def lon_dec_to_lonref(lon):
    if(lon > 0):
        gpsref = "East"
    else:
        gpsref = "West"
    return(gpsref)

def box_iso_to_iso(box_iso):
    return(int(box_iso))

def shot_iso_to_isospeed(shot_iso):
    return(int(shot_iso))

def find_filename(shot_number,file_dict):
    for filename, extracted_shotnumber in file_dict.items():
      if int(shot_number) == extracted_shotnumber:
        return(filename)
    return("null")

def convert_photoexifcsv_to_json(shots):
    exif_shots = []
    file_dict = load_file_dict(path)
    for shot in shots:
        exif_shot = {}
        datetimeoriginal = date_to_DateTimeOriginal(shot.pop("Date"))
        exposure_time = exposure_time_to_ExposureTime(shot.pop("Speed"))
        fnumber = fnumber_to_FNumber(shot.pop("Aperture"))
        focal_length = focal_length_to_FocalLength(shot.pop("Focal"))
        latitude = lat_dec_to_lat_dms(float(shot.pop("Latitude")))
        longitude = lon_dec_to_lon_dms(float(shot.pop("Longitude")))
        camera = shot.pop("Camera")
        make = camera.split(maxsplit=1)[0]
        try:
          model = camera.split(maxsplit=1)[1]
        except:
          model = "unkown"
        film_name = shot.pop("Film type")
        notes = str(shot.pop("Comment"))
        iso = box_iso_to_iso(shot.pop("ASA value"))
        imagenumber = shot.pop("Number")
        lensmodel = shot.pop("Lens")
        documentname = film_name

        exif_shot["DateTimeOriginal"] = datetimeoriginal
        exif_shot["ExposureTime"] = exposure_time
        exif_shot["FNumber"] = fnumber
        exif_shot["FocalLength"] = focal_length
        exif_shot["GPSLatitude"] = latitude
        exif_shot["GPSLatitudeRef"] = latitude
        exif_shot["GPSLongitude"] = longitude
        exif_shot["GPSLongitudeRef"] = longitude
        exif_shot["Make"] = make
        exif_shot["Model"] = model
        exif_shot["LensModel"] = lensmodel
        exif_shot["Notes"] = notes
        exif_shot["Description"] = film_name
        exif_shot["ISO"] = iso
        exif_shot["SensitivityType"] = 3 # for "ISO Speed"
        exif_shot["FileSource"] = 1 # for "FilmScanner"
        exif_shot["ImageNumber"] = imagenumber
        exif_shot["SourceFile"] = "./" + str(file_dict.get(str(int(imagenumber))))
        exif_shot["Software"] = version
        exif_shot["SpectralSensitivity"] = film_name
        exif_shot["DocumentName"] = documentname 

        exif_shot.update(shot)
        exif_shots.append(exif_shot)

    return(exif_shots)


def main():
    csv_file = get_photoexifcsv_filename(path)
    print(csv_file)
    shots = load_photoexifcsv(path + "/" + csv_file)
    exif_shots = convert_photoexifcsv_to_json(shots)
    with open(str(path +"/" + "converted.json"),"w") as file:
        json.dump(exif_shots,file,indent = 2)
if __name__=="__main__":
    path = str(sys.argv[1])
    main()
    

