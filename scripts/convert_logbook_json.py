from datetime import datetime
import math
import json
import sys
import os
import re

version = "logbook_json_exif_merge v0.1"

def get_logbookjson_filename(filepath):
   for x in os.listdir(filepath):
           if x.endswith("json"):
               json_file = x
   return(json_file)


def load_logbookjson(json_file):
    with open(json_file,"r") as file:
        logbook_json = json.load(file)

        shots = logbook_json.pop("shots")

        for shot in shots:
            shot.update(logbook_json)

    return(shots)

def load_file_dict(filepath):
    file_dict ={}
    for x in os.listdir(filepath):
        if x.endswith("tif"):
            shot_number = str(int(list(filter(None,re.findall("\d*",x)))[0]))
            file_dict[shot_number] = x
    return(file_dict)

def fnumber_to_FNumber(fnumber):
    return(fnumber)

def date_to_DateTimeOriginal(date_str):
    exif_date = datetime.strptime(date_str,"%Y-%m-%dT%H:%M:%SZ").strftime("%Y:%m:%d %H:%M:%S")
    return(exif_date)


def exposure_time_to_ExposureTime(time):
    return(time)

def focal_length_to_FocalLength(focal_length):
    return(focal_length)

def focal_length_to_FocalLengthIn35mmFormat(focal_length,film_format):
    if(film_format == "135"):
        return(focal_length)
    elif(film_format == "6x7"):
        return(int(focal_length) * 0.4692)
    else:
        return("null")

def dec_to_dms(dec):

    d = math.floor(dec)
    m = math.floor((dec-d) * 60)
    s = round((dec - d - m/60) * 3600)

    return(str(d) + "deg " + str(m) + "' " + str(s) + '" ')

def lat_dec_to_lat_dms(lat):
    if(lat > 0):
        dms_lat = dec_to_dms(lat) + "N"
    else:
        dms_lat = dec_to_dms(lat) + "S"
    return(dms_lat)

def lon_dec_to_lon_dms(lon):
    if(lon > 0):
        dms_lon = dec_to_dms(lon) + "E"
    else:
        dms_lon = dec_to_dms(lon) + "W"
    return(dms_lon)

def lat_dec_to_latref(lat):
    if(lat > 0):
        gpsref = "North"
    else:
        gpsref = "South"
    return(gpsref)

def lon_dec_to_lonref(lon):
    if(lon > 0):
        gpsref = "East"
    else:
        gpsref = "West"
    return(gpsref)

def box_iso_to_iso(box_iso):
    return(int(box_iso))

def shot_iso_to_isospeed(shot_iso):
    return(int(shot_iso))

def find_filename(shot_number,file_dict):
    for filename, extracted_shotnumber in file_dict.items():
      if int(shot_number) == extracted_shotnumber:
        return(filename)
    return("null")

def convert_logbook_to_json(shots):
    exif_shots = []
    file_dict = load_file_dict(path)
    for shot in shots:
        exif_shot = {}
        datetimeoriginal = date_to_DateTimeOriginal(shot.pop("date"))
        exposure_time = exposure_time_to_ExposureTime(shot.pop("exposure_time"))
        fnumber = fnumber_to_FNumber(shot.pop("fnumber"))
        focal_length = focal_length_to_FocalLength(shot.pop("focal_length"))
        latitude = lat_dec_to_lat_dms(shot.pop("latitude"))
        longitude = lon_dec_to_lon_dms(shot.pop("longitude"))
        make = shot.pop("camera_make")
        model = shot.pop("camera_model")
        film_name = shot.pop("film name")
        notes = str(shot.pop("notes"))
        iso = box_iso_to_iso(shot.pop("box_iso"))
        isospeed = shot_iso_to_isospeed(shot.pop("shot_iso"))
        focallengthin35mmformat = focal_length_to_FocalLengthIn35mmFormat(focal_length,shot["film_format"])
        imagenumber = shot.pop("shot_number")
        lensmake = shot.pop("lens_make")
        lensmodel = shot.pop("lens_model")
        reelname = shot.pop("roll_name")
        imageuniqueid = str(reelname) + "_" + str(imagenumber)
        usercomment = "roll_notes:" + str(shot.pop("roll_notes")) + "\ndev_notes:" + str(shot.pop("dev_notes")) + "\nload_date:" + shot.pop("load_date") + "\nunload_date:" + shot.pop("unload_date")
        documentname = film_name

        exif_shot["DateTimeOriginal"] = datetimeoriginal
        exif_shot["ExposureTime"] = exposure_time
        exif_shot["FNumber"] = fnumber
        exif_shot["FocalLength"] = focal_length
        exif_shot["GPSLatitude"] = latitude
        exif_shot["GPSLatitudeRef"] = latitude
        exif_shot["GPSLongitude"] = longitude
        exif_shot["GPSLongitudeRef"] = longitude
        exif_shot["Make"] = make
        exif_shot["Model"] = model
        exif_shot["LensMake"] = lensmake
        exif_shot["LensModel"] = lensmodel
        exif_shot["Notes"] = notes
        exif_shot["Description"] = film_name
        exif_shot["ReelName"] = reelname
        exif_shot["ISO"] = iso
        exif_shot["ISOSpeed"] = isospeed
        exif_shot["SensitivityType"] = 3 # for "ISO Speed"
        exif_shot["ImageUniqueId"] = imageuniqueid
        exif_shot["FileSource"] = 1 # for "FilmScanner"
        exif_shot["FocalLengthIn35mmFormat"] = focallengthin35mmformat
        exif_shot["ImageNumber"] = imagenumber
        exif_shot["SourceFile"] = "./" + str(file_dict.get(str(int(imagenumber))))
        exif_shot["Software"] = version
        exif_shot["UserComment"] = usercomment
        exif_shot["SpectralSensitivity"] = film_name
        exif_shot["DocumentName"] = documentname 

        exif_shot.update(shot)
        exif_shots.append(exif_shot)

    return(exif_shots)


def main():
    json_file = get_logbookjson_filename(path)
    shots = load_logbookjson(path + "/" + json_file)
    exif_shots = convert_logbook_to_json(shots)
    json_output_filename = "converted" + json_file
    print(json_output_filename)
    with open(str(path +"/" + "converted.json"),"w") as file:
        json.dump(exif_shots,file,indent = 2)
if __name__=="__main__":
    path = str(sys.argv[1])
    main()
    

