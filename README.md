# Usage

Required:

python3; exiftool


## Log shots with Logbook App (https://apps.apple.com/us/app/lightme-logbook/id1544518308)


## digitalize images

- place Images from a roll in individual folder like roll1

- files must be in tif format and contain a number corresponding to the `shot_number` in logbook or to `Number`in PhotoExif F.e image0001.tif;image0002.tif...

- dont have other numbers in the filename like 2022_image_002.tif only one number is expected by the script

## export Metadata with Logbook or PhotoExif to json

- Save the resulting logbook_json or photoexif_csv file into the same folder (roll1)
- photoexif_csv files have to be manually sanitized since if data contains newlines it will be also present in the csv file breaking it.

## run metadata conversion script

- make sure the folder contains only the images corresponding to the json file and that only one json file exists in the folder
- currently the script for logbook_json will not work if run a second time in the same folder since the output is also a json file
- if script already ran and you want to run it again for some reason, first delete `converted.json`

### For Logbook json files:
```
python convert_logbook_json.py "path/to/folder/"
```

In Batch mode:

```
find . -name *.json -execdir sh -c '(pwd && python ~/Documents/projekte/privat/logbook-json-exif-merge/scripts/convert_logbook_json.py . && echo)' \;
```

### Fot PhotoExif csv files:

- Photo exif does not sanitize its csv exports, Note which where taken with the app will not be quoted and can contain newline characters breaking the csv file. Remove unwanted newlines manually beforehand

```
python convert_photoexif_csv.py "path/to/folder/"
```

In Batch mode:

```
find . -name *.json -execdir sh -c '(pwd && python ~/Documents/projekte/privat/logbook-json-exif-merge/scripts/convert_photoexif_csv.py . && echo)' \;
```


## then write converted tags to imagefiles

!!!ORIGINAL IMAGES WILL BE OVERWRITTEN!!!!

```
exiftool -overwrite_original -json="path/to/folder/converted.json" "path/to/folder"
```

In Batchmode:

```
find . -name converted.json -execdir sh -c '(pwd && exiftool.exe -overwrite_original -json=converted.json . && echo)' \;
```



- to let exiftool create backupfiles before writing tags remove `-overwrite_original` option



